#LED Goggles#
Using 2x 16 NEOPIXEL rings inside some steampunk goggles

Powered by an arduino trinket 5v

A push button and potentiometer are used so the wearer can select the effect and adjust the chosen setting

#Effects#
Each effect should be created in isolation, when working, it should be incorporated into  `All_Effects_Button_Pot.ino`

#Folders#
Arduino IDE can only compile `.ino` files if the parent directory has the same name

#Improvements#
Pull requests and Issue creation are welcome :)

some change