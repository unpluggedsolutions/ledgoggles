#include <Adafruit_NeoPixel.h>

//Neopixel definitions
#define neoPin 10  //NeoPixel output pin
#define pixelCount 32 //number of NeoPixels in strip

//Input pins
int tempPin = A1;     // Input pin for the temperature sensor
int sensorPin = A6;   // select the input pin for the potentiometer
int switchPin = 3;    // for changing the effect
int switchState;
int switchStatePrv;

unsigned long interval=0;  // the time we need to wait
unsigned long previousMillis=0;

uint32_t currentColor;// current Color in case we need it
uint16_t currentPixel = 0;// what pixel are we operating on

int effectNumber = 1;
int effectsTotal = 6;

//colorWipe() variables
int R = 0;
int G = 0;
int B = 125;

//splitColorWipe() variables
int pixelL = 0;   //start pixel for the left end
int pixelR = 31;  //start pixel for the right end

uint32_t stripColor;
int pixelPosition;
int i;
int j;

//For allowing reverse patterns
int effectDirection = 0; // 0 is 'forward', 1 is 'reversed'

const int numReadings = 10;

int readings[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
float total = 0;                  // the running total
float average = 0;                // the average

Adafruit_NeoPixel strip = Adafruit_NeoPixel(pixelCount, neoPin, NEO_GRB + NEO_KHZ800);

void setup() {
  currentPixel = 0;
  pinMode(switchPin, INPUT_PULLUP);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
  enDisplay();
  delay(1000);
}

// Basic idea. You could reorg and pass pixel index and color as a function - or you could go through a gradient of colors.
void loop(){

//pixelCount = 32;
//dealing with the potentiometer to set the effect speed
interval = map(analogRead(sensorPin), 0, 1023, 7, 80);
if ((unsigned long)(millis() - previousMillis) >= interval) {
  previousMillis = millis();

//dealing with the button to set the effect number
switchState = digitalRead(switchPin);
if (switchState != switchStatePrv) {
  if(switchState == LOW) {
    if (effectNumber < effectsTotal) {
      effectNumber++;
    }
    else {
      effectNumber = 1;
    } 
    enDisplay();
  }
}
switchStatePrv = switchState;

//calling functions depending on the effect number
switch (effectNumber) {
  case 1:
    larsonScanner();
    //colorWipe();
    break;
  case 2:
    splitColorWipe();
    break;
  case 3:
    splitColorWipeRandom();
    break;
  case 4:
    fadeInAndOut();
    break;
  case 5:
    rainbowCycle();
    break;
  case 6:
    tadpoleChase();
    break;
  default:
    strip.begin();
    strip.show(); // Initialize all pixels to 'off'
    }
  }
}

//function to light single pixel showing which number effect has been selected
void enDisplay() {
  currentPixel = 0;
  for(i=0;i<pixelCount;i++){
    strip.setPixelColor(currentPixel,0,0,0);
    currentPixel++;
  }
  currentPixel = 0;
  for(i=0;i<effectNumber;i++){
    strip.setPixelColor(currentPixel,125,125,125);
    currentPixel++;
  }
  strip.show();
  delay(500);
  currentPixel = 0;
  for(i=0;i<pixelCount;i++){
    strip.setPixelColor(currentPixel,0,0,0);
    currentPixel++;
  }
  currentPixel=0;
  pixelL=0;
  pixelR=31;
}

//------------Effect Start--------------------------------
void larsonScanner() {
  for(i=0;i<(pixelCount+1);i++) {
    strip.setPixelColor(currentPixel-2,0,0,0);
    strip.setPixelColor(currentPixel-1,50,0,0);
    strip.setPixelColor(currentPixel,150,0,0);
    strip.setPixelColor(currentPixel+1,50,0,0);
    strip.setPixelColor(currentPixel+2,0,0,0);
  }
  if (currentPixel == pixelCount) {
    effectDirection = 0;
  }
  else if (currentPixel == 0) {
    effectDirection = 1;
  }
  
  if (effectDirection == 0) {
    currentPixel--;
  }
  else if (effectDirection == 1) {
    currentPixel++;
  }

  strip.show();
}
//------------Effect End----------------------------------


//------------Effect Start--------------------------------
void tadpoleChase() {
  for(i=0;i<pixelCount;i++) {
    strip.setPixelColor((currentPixel-(0*8))-4,0,0,0);
    strip.setPixelColor((currentPixel-(0*8))-3,0,0,20);
    strip.setPixelColor((currentPixel-(0*8))-2,0,0,30);
    strip.setPixelColor((currentPixel-(0*8))-1,0,0,60);
    strip.setPixelColor((currentPixel-(0*8)),0,0,200);

    strip.setPixelColor((currentPixel-(1*8))-4,0,0,0);
    strip.setPixelColor((currentPixel-(1*8))-3,20,0,0);
    strip.setPixelColor((currentPixel-(1*8))-2,30,0,0);
    strip.setPixelColor((currentPixel-(1*8))-1,60,0,0);
    strip.setPixelColor((currentPixel-(1*8)),200,0,0);

    strip.setPixelColor((currentPixel-(2*8))-4,0,0,0);
    strip.setPixelColor((currentPixel-(2*8))-3,0,20,0);
    strip.setPixelColor((currentPixel-(2*8))-2,0,30,0);
    strip.setPixelColor((currentPixel-(2*8))-1,0,60,0);
    strip.setPixelColor((currentPixel-(2*8)),0,200,0);

    strip.setPixelColor((currentPixel-(3*8))-4,0,0,0);
    strip.setPixelColor((currentPixel-(3*8))-3,0,10,10);
    strip.setPixelColor((currentPixel-(3*8))-2,0,15,15);
    strip.setPixelColor((currentPixel-(3*8))-1,0,30,30);
    strip.setPixelColor((currentPixel-(3*8)),0,100,100);

    strip.setPixelColor((currentPixel-(4*8))-4,0,0,0);
    strip.setPixelColor((currentPixel-(4*8))-3,10,0,10);
    strip.setPixelColor((currentPixel-(4*8))-2,15,0,15);
    strip.setPixelColor((currentPixel-(4*8))-1,30,0,30);
    strip.setPixelColor((currentPixel-(4*8)),100,0,100);

    strip.setPixelColor((currentPixel-(5*8))-4,0,0,0);
    strip.setPixelColor((currentPixel-(5*8))-3,10,10,0);
    strip.setPixelColor((currentPixel-(5*8))-2,15,15,0);
    strip.setPixelColor((currentPixel-(5*8))-1,30,30,0);
    strip.setPixelColor((currentPixel-(5*8)),100,100,0);
  }
  if(currentPixel == (pixelCount+44)){
    currentPixel=0;
  }
  else {
    currentPixel++;
  }
  strip.show();
}
//------------Effect End----------------------------------

//------------Effect Start--------------------------------
void colorWipe(){
  if (effectDirection == 0) {
    
    switch (i) {
      case 1:
        R = 0;  //random(0,256);
        G = 0;  //random(0,256);
        B = 125;  //random(0,256);
        break;
      case 2:
        R = 0;  //random(0,256);
        G = 125;  //random(0,256);
        B = 0;  //random(0,256);
        break;
      case 3:
        R = 125;  //random(0,256);
        G = 0;  //random(0,256);
        B = 0;  //random(0,256);
        break;
      case 4:
        R = 125;  //random(0,256);
        G = 125;  //random(0,256);
        B = 0;  //random(0,256);
        break;
      case 5:
        R = 125;  //random(0,256);
        G = 0;  //random(0,256);
        B = 125;  //random(0,256);
        break;
      case 6:
        R = 0;  //random(0,256);
        G = 125;  //random(0,256);
        B = 125;  //random(0,256);
        break;
      case 7:
        R = random(0,125);
        G = random(0,125);
        B = random(0,125);
        break;
      default:
        R = 0;
        G = 0;
        B = 0;
    }
   
    currentColor = strip.Color(R,G,B);
    
    strip.setPixelColor(currentPixel,currentColor);
    strip.show();
    currentPixel++;
        
    if(currentPixel == pixelCount){
      effectDirection = 1;
      i = random(1,8);
      }
  }
  else if (effectDirection == 1) {

    switch (i) {
      case 1:
        R = 0;  //random(0,256);
        G = 0;  //random(0,256);
        B = 125;  //random(0,256);
        break;
      case 2:
        R = 0;  //random(0,256);
        G = 125;  //random(0,256);
        B = 0;  //random(0,256);
        break;
      case 3:
        R = 125;  //random(0,256);
        G = 0;  //random(0,256);
        B = 0;  //random(0,256);
        break;
      case 4:
        R = 125;  //random(0,256);
        G = 125;  //random(0,256);
        B = 0;  //random(0,256);
        break;
      case 5:
        R = 125;  //random(0,256);
        G = 0;  //random(0,256);
        B = 125;  //random(0,256);
        break;
      case 6:
        R = 0;  //random(0,256);
        G = 125;  //random(0,256);
        B = 125;  //random(0,256);
        break;
      case 7:
        R = random(0,125);
        G = random(0,125);
        B = random(0,125);
        break;
      default:
        R = 0;
        G = 0;
        B = 0;
    }

    currentColor = strip.Color(R,G,B);

    strip.setPixelColor(currentPixel,currentColor);
    strip.show();
       
    if(currentPixel == 0){
      effectDirection = 0;
      i = random(1,8);
      }
    currentPixel--;
  }
}
//------------Effect End----------------------------------

  
//------------Effect Start--------------------------------
void splitColorWipe() {
  if (effectDirection == 0) {
        
    strip.setPixelColor(pixelL,125,0,0);
    strip.setPixelColor(pixelR,125,0,0);
    strip.show();

    pixelR--;
    pixelL++; 
    if (pixelL == pixelCount/2) {
      effectDirection = 1;
      }
     
  }
  else if (effectDirection == 1) {
    //colorL = strip.Color(255,0,0);
    //colorR = strip.Color(0,0,255);
    
    strip.setPixelColor(pixelL,0,0,125);
    strip.setPixelColor(pixelR,0,0,125);
    strip.show();

    if (pixelL == 0) {
      effectDirection = 0;
    }
    pixelL--;
    pixelR++; 
  }
}
//------------Effect End----------------------------------


//------------Effect Start--------------------------------
void splitColorWipeRandom() {
  if (effectDirection == 0) {
        
    //strip.setPixelColor(pixelL,255,0,0);
    //strip.setPixelColor(pixelR,255,0,0);
    strip.setPixelColor(pixelL,random(0,255),random(0,255),random(0,255));
    strip.setPixelColor(pixelR,random(0,255),random(0,255),random(0,255));
    strip.show();

    pixelR--;
    pixelL++; 
    if (pixelL == pixelCount/2) {
      effectDirection = 1;
      }
     
  }
  else if (effectDirection == 1) {
    //colorL = strip.Color(255,0,0);
    //colorR = strip.Color(0,0,255);
    
    //strip.setPixelColor(pixelL,0,0,255);
    //strip.setPixelColor(pixelR,0,0,255);
    strip.setPixelColor(pixelL,random(0,255),random(0,255),random(0,255));
    strip.setPixelColor(pixelR,random(0,255),random(0,255),random(0,255));
    strip.show();

    if (pixelL == 0) {
      effectDirection = 0;
    }
    pixelL--;
    pixelR++; 
  }
}
//------------Effect End----------------------------------

//------------Effect Start--------------------------------
void rainbowCycle() {
  if (effectDirection == 0) {
  pixelPosition = map(currentPixel, 0, pixelCount, 0, 255);
  if ((pixelPosition >= 0) && (pixelPosition < 85)) {
    R = map(pixelPosition,0,85,255,85);
    G = map(pixelPosition,0,85,0,170);
    B = 0;
    stripColor = strip.Color(R, G, B);
  }
  else if ((pixelPosition >= 85) && (pixelPosition < 170)) {
    R = 0;
    G = map(pixelPosition,85,170,255,85);
    B = map(pixelPosition,85,170,0,170);
    stripColor = strip.Color(R, G, B);
  }
  else if ((pixelPosition >= 170) && (pixelPosition < 255)) {
    R = map(pixelPosition,170,255,0,170);
    G = 0;
    B = map(pixelPosition,170,255,255,85);
    stripColor = strip.Color(R, G, B);
  }

    strip.setPixelColor(currentPixel, stripColor);
    strip.show();
    currentPixel++;
    if (currentPixel == pixelCount) {
      effectDirection = 1;
    }
  }
  if (effectDirection == 1) {
    pixelPosition = map(currentPixel, 0, pixelCount, 0, 255);
  if ((pixelPosition >= 0) && (pixelPosition < 85)) {
    R = map(pixelPosition,0,85,170,0);
    G = 0;
    B = map(pixelPosition,0,85,85,255);
    stripColor = strip.Color(R, G, B);
  }
  else if ((pixelPosition >= 85) && (pixelPosition < 170)) {
    R = 0;
    G = map(pixelPosition,85,170,85,255);
    B = map(pixelPosition,85,170,170,0);
    stripColor = strip.Color(R, G, B);
  }
  else if ((pixelPosition >= 170) && (pixelPosition < 255)) {
    R = map(pixelPosition,170,255,85,255);
    G = map(pixelPosition,170,255,170,0);
    B = 0;
    stripColor = strip.Color(R, G, B);
  }

    strip.setPixelColor(currentPixel, stripColor);
    strip.show();
    
    if (currentPixel == 0) {
      effectDirection = 0;
    }
    currentPixel--;
  }
}
//------------Effect End----------------------------------


//------------Effect Start--------------------------------
void fadeInAndOut() {
  if (effectDirection == 0) {
    if (j < pixelCount) {
      R = 125;
      G = 125;
      B = 125;
      stripColor = strip.Color(R, G, B);
      strip.setPixelColor(currentPixel, stripColor);
    }
    else if (j >= pixelCount) {
      j = 0;
    }
    
    currentPixel += 2;
    if (currentPixel >= pixelCount) {
      effectDirection = 1;
      
      j++;
      strip.show();
    }
   }
  else if (effectDirection == 1) {
   if (j < pixelCount) {
      R = 0;
      G = 0;
      B = 0;
      stripColor = strip.Color(R, G, B);
      strip.setPixelColor(currentPixel, stripColor);
    }
    else if (j >= pixelCount) {
      j = 0;
    }
    
    
    if (currentPixel <= 0) {
     effectDirection = 0;
     
     // j++;
     strip.show();
    }
    currentPixel--;
   }
}
//------------Effect End----------------------------------

